# ORLocation

[![CI Status](http://img.shields.io/travis/Maxim Soloviev/ORLocation.svg?style=flat)](https://travis-ci.org/Maxim Soloviev/ORLocation)
[![Version](https://img.shields.io/cocoapods/v/ORLocation.svg?style=flat)](http://cocoapods.org/pods/ORLocation)
[![License](https://img.shields.io/cocoapods/l/ORLocation.svg?style=flat)](http://cocoapods.org/pods/ORLocation)
[![Platform](https://img.shields.io/cocoapods/p/ORLocation.svg?style=flat)](http://cocoapods.org/pods/ORLocation)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ORLocation is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ORLocation"
```

## Author

Maxim Soloviev, maxim@omega-r.com

## License

ORLocation is available under the MIT license. See the LICENSE file for more info.
